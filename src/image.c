#include <string.h>
#include <SDL2/SDL_image.h>
#include "image.h"
#include "rect.h"
#include "log.h"

struct image image_init(const char* path)
{
    struct image img;
    img.valid = true;
    img.texture = NULL;
    img.ratio = 1;
    img.path = lstr_clone(path);
    if (strstr(path, ".png"))
        img.type = image_png;
    else
        img.type = image_jpeg;
    return img;
}

void image_load_texture(struct image *image) {
    if (!image->valid || image->texture)
        return;
    image->texture = IMG_LoadTexture(g_rend.rend, image->path);
    int w = 0, h = 0;
    if (image->texture == NULL) {
        WARN("Failed loading image %s: %s", image->path,
             IMG_GetError());
        image->valid = false;
    } else {
        SDL_QueryTexture(image->texture, NULL, NULL, &w, &h);
    }
    image->ratio = (float)w / h;
}

struct image image_clone(struct image img)
{
    return image_init(img.path);
}

void image_drop(struct image *image) {
    lstr_drop(&image->path);
    if (image->valid && image->texture)
        SDL_DestroyTexture(image->texture);
}

struct rect image_get_bound(struct image *image,
        const struct rect *rect) {
    /* find maximum (width, height) which
     * width <= .width, height <= .height
     * and width / height == ratio */
    int imgh = rect->w / image->ratio;
    int imgw = rect->h * image->ratio;
    struct rect out = { 0 };
    if (imgh <= rect->h) {
        out.w = rect->w;
        out.h = imgh;
    } else {// if (imgw <= rect->w) {
        out.w = imgw;
        out.h = rect->h;
    }
    int xoffset = (rect->w - out.w) / 2;
    int yoffset = (rect->h - out.h) / 2;
    out.x = rect->x + xoffset;
    out.y = rect->y + yoffset;
    return out;
}

void image_draw_pdf(struct image *image,
        const struct rect *rect, HPDF_Doc pdf, HPDF_Page pp) {
    if (!image->valid)
        return;
    image_load_texture(image);
    HPDF_Image i;
    if (image->type == image_png) {
        i = HPDF_LoadPngImageFromFile(pdf, image->path);
    } else {
        i = HPDF_LoadJpegImageFromFile(pdf, image->path);
    }
    if (i == NULL) PANIC("pdf: Failed loading image");
    const struct rect bnd = image_get_bound(image, rect);\
    HPDF_Page_DrawImage(pp, i, bnd.x, g_rend.height - bnd.y - bnd.h, bnd.w, bnd.h);
}

void image_draw(struct image *image, const struct rect *rect) {
    if (!image->valid)
        return;
    image_load_texture(image);
    const struct rect bnd = image_get_bound(image, rect);\
    SDL_RenderCopy(g_rend.rend, image->texture, NULL, (SDL_Rect*)&bnd);
}

