#pragma once
#ifndef SLIDE_H
#define SLIDE_H

#include <stddef.h>
#include <hpdf.h>
#include "frame.h"
#include "page.h"

#define l_con slide
#define l_val page
#define l_valclone page_clone
#define l_valdrop page_drop
#include <ena/dynarray.h>

slide slide_parse(const char *path);

#endif /* SSLIDE_H */

