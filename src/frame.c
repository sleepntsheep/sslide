#include "frame.h"
#include "font.h"
#include "log.h"
#include "config.h"
#include "rect.h"
#include "string.h"
#include "util.h"
#include <SDL2/SDL_ttf.h>

struct frame frame_clone(struct frame f)
{
    struct frame w = { 0 };
    w.type = f.type;
    if (f.type == frame_text)
        w.lines = lstr_array_clone(f.lines);
    else if (f.type == frame_image)
        w.image = image_clone(f.image);
    w.font = f.font;
    w.x = f.x;
    w.y = f.y;
    w.w = f.w;
    w.h = f.h;
    return w;
}

// x * g_rend.width / 100
// x * ((100 + magrinx) * g_rend.width / 100) / 100
// rx = fx (g_rend.width + marginx g_rend.width/100) / 100
struct rect frame_get_rect_px(const struct frame *frame) {
    struct rect rm;
    rm.x = g_rend.width * margin_x / 100;
    rm.y = g_rend.height * margin_y / 100;
    rm.w = g_rend.width - 2 * rm.x;
    rm.h = g_rend.height - 2 * rm.y;

    return (struct rect) {
        .x = rm.x + frame->x * rm.w / 100,
        .y = rm.y + frame->y * rm.h / 100,
        .w = frame->w * rm.w / 100,
        .h = frame->h * rm.h / 100,
    };
}

struct frame frame_text_init(lstr_array lines, int x, int y, int w,
                    int h) {
    struct frame f;
    f.type = frame_text;
    f.x = x;
    f.y = y;
    f.w = w;
    f.h = h;
    f.lines = lstr_array_clone(lines);
    if (lines.length) {
        size_t joined_len = 0;
        for (size_t j = 0; j < lines.length; j++)
            joined_len += lstr_length(lines.data[j]);
        lstr joined = lstr_make("");
        joined = lstr_realloc(joined, joined_len + 1);
        for (size_t j = 0; j < lines.length; j++)
            joined = lstr_cat(joined, lines.data[j]);
        f.font = font_covering_ttf(joined);
        if (f.font == NULL)
            WARN("Failed fetching font for frame");
        lstr_drop(&joined);
    }
    return f;
}

struct frame frame_image_init(struct image image, int x, int y, int w, int h) {
    struct frame f;
    f.type = frame_image;
    f.image = image_clone(image);
    f.x = x;
    f.y = y;
    f.w = w;
    f.h = h;
    return f;
}

void frame_drop(struct frame *frame) {
    if (frame->type == frame_text)
        lstr_array_drop(&frame->lines);
    else if (frame->type == frame_image)
        image_drop(&frame->image);
}

int frame_find_font_size(struct frame *frame, int *size, int *text_width,
                         int *text_height) {
    /* binary search to find largest font size that fit in frame */
    const struct rect fr = frame_get_rect_px(frame);
    char *path = frame->font;
    int bl = 0, br = 256;
    int linecount = frame->lines.length;
    int lfac = linespacing * (linecount - 1);

    while (bl <= br) {
        int m = bl + (br - bl) / 2;
        int text_w = 0, text_h = 0, line_h = 0, line_w = 0;
        TTF_Font *font = TTF_OpenFont(path, m);
        if (!font) {
            WARN("Failed to open font %s: %s", path, TTF_GetError());
            return -1;
        }
        for (int i = 0; i < linecount; i++) {
            if (TTF_SizeUTF8(font, frame->lines.data[i], &line_w, &line_h) == -1) {
                WARN("Failed to get size: %s", TTF_GetError());
            }
            text_w = maxi(text_w, line_w);
            text_h += line_h;
        }
        text_h += lfac;
        if (text_h <= fr.h && text_w <= fr.w) {
            *size = m;
            *text_width = text_w,
            *text_height = text_h,
            bl = m + 1;
        } else {
            br = m - 1;
        }
        TTF_CloseFont(font);
    }
    return 0;
}

void frame_draw(struct frame *frame) {
    const struct rect fr = frame_get_rect_px(frame);
    if (frame->type == frame_text && frame->lines.length) {
        int text_width = 0, text_height = 0, fontsize = 0;
        if (frame_find_font_size(frame, &fontsize, &text_width,
                                 &text_height) != 0) {
            return;
        }
        TTF_Font *font = TTF_OpenFont(frame->font, fontsize);
        int line_height = TTF_FontHeight(font);
        int xoffset = (fr.w - text_width) / 2;
        int yoffset = (fr.h - text_height) / 2;

        for (size_t i = 0; i < frame->lines.length; i++) {
            SDL_Surface *textsurface =
                TTF_RenderUTF8_Blended(font, frame->lines.data[i], fg);
            SDL_Texture *texttexture =
                SDL_CreateTextureFromSurface(g_rend.rend, textsurface);
            int linew, lineh;
            SDL_QueryTexture(texttexture, NULL, NULL, &linew, &lineh);

            SDL_RenderCopy(g_rend.rend, texttexture, NULL, &(SDL_Rect) {
                .x = fr.x + xoffset,
                .y = fr.y + (line_height + linespacing) * i +
                     yoffset,
                .w = linew,
                .h = lineh,
            });

            SDL_FreeSurface(textsurface);
            SDL_DestroyTexture(texttexture);
        }
        TTF_CloseFont(font);
    } else if (frame->type == frame_image) {
        image_draw(&frame->image, &fr);
    }
}

int frame_find_font_size_pdf(HPDF_Doc pdf, struct frame *frame, int *size, int *text_width,
                         int *text_height) {
    /* binary search to find largest font size that fit in frame */
    const struct rect fr = frame_get_rect_px(frame);
    int bl = 0, br = 256;
    int lfac = linespacing * (frame->lines.length - 1);

    while (bl <= br) {
        int m = bl + (br - bl) / 2;
        int text_w = 0, text_h = 0;

        const char *font_name = HPDF_LoadTTFontFromFile(pdf, frame->font, true);
        HPDF_Font hfont = HPDF_GetFont(pdf, font_name, "UTF-8");

        int line_height = HPDF_Font_GetCapHeight(hfont) * m / 1000.0f;
        if (0 == line_height) PANIC("HPDF failed getting font height");

        for (size_t i = 0; i < frame->lines.length; i++) {
            HPDF_TextWidth line_width = HPDF_Font_TextWidth(hfont, (unsigned char*)frame->lines.data[i], lstr_length(frame->lines.data[i]));
            text_w = maxi(text_w, line_width.width * m / 1000.0f);
            text_h += line_height;
        }

        text_h += lfac;
        if (text_h <= fr.h && text_w <= fr.w) {
            *size = m;
            *text_width = text_w,
            *text_height = text_h,
            bl = m + 1;
        } else {
            br = m - 1;
        }
    }
    return 0;
}

void frame_draw_pdf(struct frame *frame, HPDF_Doc pdf, HPDF_Page pp) {
    const struct rect fr = frame_get_rect_px(frame);
    if (frame->type == frame_text && frame->lines.length) {
        int text_width = 0, text_height = 0, fontsize = 0;
        if (frame_find_font_size_pdf(pdf, frame, &fontsize, &text_width, &text_height) != 0)
        {
            WARN("Failed finding optimal font size");
            return;
        }
        const char *font_name = HPDF_LoadTTFontFromFile(pdf, frame->font, true);

        HPDF_Font hfont = HPDF_GetFont(pdf, font_name, "UTF-8");
        HPDF_Page_SetFontAndSize(pp, hfont, fontsize);

        int line_height = HPDF_Font_GetCapHeight(hfont) * fontsize / 1000.0f;
        if (0 == line_height) PANIC("HPDF failed getting font height");

        int xoffset = maxi((fr.w - text_width) / 2, 0);
        int yoffset = maxi((fr.h - text_height) / 2, 0);

        for (size_t i = 0; i < frame->lines.length; i++) {
            HPDF_Page_BeginText(pp);
            HPDF_Page_TextOut(pp, fr.x + xoffset,
                                  g_rend.height - (fr.y + (line_height + linespacing) * (i + 1) + yoffset)
                                  , frame->lines.data[i]);
            HPDF_Page_EndText(pp);
        }

    } else if (frame->type == frame_image) {
        image_draw_pdf(&frame->image, &fr, pdf, pp);
    }   
}

