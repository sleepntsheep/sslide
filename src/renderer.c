#include "renderer.h"
#include "log.h"
#include "image.h"

struct renderer g_rend = { 0 };

void renderer_init(const char *title, unsigned width, unsigned height) {
    if (SDL_Init(SDL_INIT_VIDEO) < 0)
        PANIC("SDL_Init: %s\n", SDL_GetError());
    if (TTF_Init() < 0)
        PANIC("TTF_Init: %s\n", TTF_GetError());
    if (IMG_Init(IMG_INIT_JPG | IMG_INIT_PNG | IMG_INIT_WEBP) < 1)
        PANIC("Img_INIT: %s\n", IMG_GetError());
    g_rend.width = width;
    g_rend.height = height;
    g_rend.win = SDL_CreateWindow(title, SDL_WINDOWPOS_CENTERED,
            SDL_WINDOWPOS_CENTERED, width, height, SDL_WINDOW_RESIZABLE);
    if (NULL == g_rend.win)
        PANIC("Failed creating window: %s", SDL_GetError());
    g_rend.rend = SDL_CreateRenderer(g_rend.win, -1, 0);
    if (NULL == g_rend.rend)
        PANIC("Failed creating renderer: %s", SDL_GetError());
    INFO("Initialized window\n");
}

void renderer_deinit(void) {
    SDL_DestroyRenderer(g_rend.rend);
    SDL_DestroyWindow(g_rend.win);
    SDL_Quit();
    TTF_Quit();
    IMG_Quit();
}

