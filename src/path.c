#include "path.h"
#include "util.h"
#include <stdlib.h>
#include <stddef.h>
#include <stdio.h>
#include <string.h>

#ifdef _WIN32
/* needed for tinyfiledialog and PathIsRelative */
#include <shlwapi.h>
#include <windows.h>
#else
/* needed for getting home directory (getpwuid)
   in case $HOME isn't set */
#include <pwd.h>
#include <unistd.h>
#endif
#include "string.h"

bool path_isrelative(const char *path) {
    if (!path) return false;
#ifndef _WIN32 /* path beginning with root is absolute path */
    return path[0] != '/';
#else /* relative path in windows is complicated */
    return PathIsRelative(path);
#endif
}

lstr path_gethome(void) {
#ifdef _WIN32
    char *p = getenv("USERPROFILE");
    if (p) return lstr_make(p);
    return lstr_asprintf("%s/%s", getenv("HOMEDRIVE"), getenv("HOMEPATH"));
#else
    char *p = getenv("HOME");
    if (!p) p = getpwuid(getuid())->pw_dir;
    return lstr_make(p);
#endif
    return NULL;
}

lstr path_dirname(const char *path) {
    if (!path) return NULL;
#ifdef _WIN32
    /* note - im not sure if this WIN32 version work */
    lstr s = lstr_make(path);
    char *p = strrchr(s, '/');
    if (p) {
        *p = 0;
    } else {
        p = strrchr(s, '\\');
        if (p) *p = 0;
    }
    return s;
#else
    char *s = lstr_make(path);
    char *p = strrchr(s, '/');
    if (p) *p = 0;
    return s;
#endif
}

