#pragma once
#include "string.h"
#include "util.h"
#include <hpdf.h>
#include "image.h"
#include "common.h"

enum frame_type {
    frame_none,
    frame_text,
    frame_image,
};

struct frame {
    int type;
    struct image image;
    lstr_array lines;
    char *font;
    int x, y, w, h;
};

struct frame frame_clone(struct frame frame);
void frame_drop(struct frame *frame);
struct frame frame_text_init(lstr_array lines, int, int, int, int);
struct frame frame_image_init(struct image image, int, int, int, int);
void frame_draw(struct frame *frame);
void frame_draw_pdf(struct frame *frame,
        HPDF_Doc pdf, HPDF_Page pp);
struct rect frame_get_rect_px(const struct frame *frame);
int frame_find_font_size(struct frame *frame, int *size, int *text_width,
                         int *text_height);

