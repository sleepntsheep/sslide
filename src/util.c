#include <stdarg.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "util.h"

int mini(int a, int b)
{
    return a < b ? a : b;
}

int maxi(int a, int b)
{
    return a > b ? a : b;
}


