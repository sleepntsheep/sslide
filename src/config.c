#include "config.h"

const int margin_x = 10;
const int margin_y = 10;
const int progress_bar_height = 10;
const int linespacing = 5;
SDL_Color bg = {255, 255, 255, 255};
SDL_Color fg = {0, 0, 0, 255};

