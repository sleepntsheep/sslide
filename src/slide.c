#include <errno.h>
#include "slide.h"
#include "path.h"
#include "log.h"
#include "common.h"

slide slide_parse(const char *path)
{
    //
    // States:
    //  0 -> start
    //  1 -> got geometry, waiting for content
    //  2 -> text frame
    //  3 -> image frame
    //  4 -> text frame1
    //  5 -> image frame1
    //  6 -> after frame1
    //
    //  0|6 + geo -> 1
    //  0|6 + text -> 4
    //  0|6 + image -> 5
    //
    //  1 + text -> 2
    //  1 + image -> 3
    //  3 + text -> error 
    //  2 + image -> error
    //
    //  2 | 3 + '\n' -> 0
    //  4 | 5 + '\n' -> 6
    //
    //  2 | 3 + page_terminator -> push frame, push page
    //  0 + page_terminator -> push page
    //  6 + page_terminator -> error
    //

    slide slide = slide_init();

    int state = 0;
    int line = 1;
    char buf[4096];
    lstr path_dir = NULL;
    FILE *fp = NULL;

    if (0 != strcmp(path, "-")) {
        fp = fopen(path, "r");
        path_dir = path_dirname(path);
    } else {
        fp = stdin;
        path_dir = path_dirname(".");
    }

    lstr_array text = lstr_array_init();
    page page = page_init();
    int x = 0, y = 0, w = 100, h = 100;
    struct image image = {0};

    for (; fgets(buf, sizeof buf, fp); line++)
    {
        size_t buflen = strlen(buf);
        if (buflen > 0)
            for (size_t i = buflen; i > 0 && strchr("\n", buf[i-1]); i--)
                buf[i-1] = 0;

        enum buf_type {
            line_geometry,
            line_text,
            line_image,
            line_terminate_frame,
            line_terminate_page,
        } line_type;

        ////////// parse
        if (buf[0] == '#')                        continue;
        else if (buf[0] == '%')                   line_type = line_image;
        else if (buf[0] == ';')                   line_type = line_geometry;
        else if (buf[0] == '\0')                  line_type = line_terminate_frame;
        else if (buf[0] == '@' && buf[1] == '\0') line_type = line_terminate_page;
        else                                      line_type = line_text;

        //////// transition

        if (line_type == line_geometry)
        {
            if (0 != state && 6 != state) PANIC("unexpected geometry at line %d", line);
            state = 1;
            if (!strcmp(buf, ";f")) x = y = 0, w = h = 100;
            else if (sscanf(buf, ";%d;%d;%d;%d", &x, &y, &w, &h) != 4) PANIC("invalid geometry at line %d");
        }

        else if (line_type == line_text)
        {
            if (3 == state || 5 == state) PANIC("unexpected text at line %d", line);
            if (1 == state || 2 == state) state = 2;
            else if (0 == state || 4 == state || 6 == state) state = 4;
            lstr_array_push(&text, buf);
        }

        else if (line_type == line_image)
        {
            if (1 != state && 0 != state && 6 != state) PANIC("unexpected text at line %d", line);
            lstr path = NULL;
            if (path_isrelative(buf + 1))
                path = lstr_asprintf("%s/%s", path_dir, buf + 1);
            else
                path = lstr_make(buf + 1);

            image = image_init(path);

            if (0 == state || 5 == state || 6 == state) state = 5;
            else if (1 == state) state = 3;

            lstr_drop(&path);
        }

        else if (line_type == line_terminate_frame || (line_type == line_terminate_page && state))
        {
            if (0 == state || 6 == state) continue;

            struct frame nf = {0};

            if (state == 2 || state == 4)
            {
                nf = frame_text_init(text, x, y, w, h);
                lstr_array_drop(&text);
                text = lstr_array_init();
            }
            else if (state == 3 || state == 5)
            {
                nf = frame_image_init(image, x, y, w, h);
                image_drop(&image);
            }
            else
                PANIC("unexpected frame terminator");

            page_push(&page, nf);
            frame_drop(&nf);

            x = y = 0, w = h = 100;
            if (4 == state || 5 == state)
            {
                slide_push(&slide, page);
                page_drop(&page);
                page = page_init();
                state = 6;
            }
            else state = 0;
        }

        if (line_type == line_terminate_page)
        {
            if (0 != state && 2 != state && 3 != state) PANIC("unexpected page terminator at line %d", line);
            slide_push(&slide, page);
            page_drop(&page);
            page = page_init();
            state = 0;
        }
    }

    if (stdin != fp)
        fclose(fp);

    lstr_array_drop(&text);
    lstr_drop(&path_dir);
    page_drop(&page);

    return slide;
}


