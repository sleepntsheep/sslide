#pragma once
#ifndef RENDERER_H
#define RENDERER_H
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_image.h>
#include <hpdf.h>
#include "common.h"

struct renderer {
    SDL_Renderer *rend;
    SDL_Window *win;
    unsigned width;
    unsigned height;
};

extern struct renderer g_rend;

void renderer_init(const char *title, unsigned width, unsigned height);
void renderer_deinit(void);

#endif /* RENDERER_H */

