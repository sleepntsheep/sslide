#pragma once
#ifndef RECT_H_
#define RECT_H_

struct rect {
    int x, y, w, h;
};

struct rect rect_make(int x, int y, int w, int h);

#endif /* RECT_H_ */
