#pragma once

#ifndef COMMON_H_
#define COMMON_H_

#include <ena/lstr.h>

#define l_con lstr_array
#define l_val lstr
#define l_valclone lstr_clone
#define l_valdrop lstr_drop
#include <ena/dynarray.h>

#define RETURN_DEFER(x) do { result = (x); goto defer; } while (0)

#endif

