#include <fontconfig/fontconfig.h>
#include <stddef.h>
#include <string.h>
#include <stdlib.h>
#include "font.h"
#include "string.h"
#include "log.h"
#include "common.h"

char *font_covering_ttf(const char *text) {
    FcCharSet *cs = NULL;
    FcPattern *pat = NULL;
    FcFontSet *matches = NULL;
    char *result = NULL;

    {
        cs = FcCharSetCreate();
        if (!cs) RETURN_DEFER(NULL);
        size_t textlen = strlen(text);
        for (size_t i = 0; i < textlen; ) {
            FcChar32 cs4 = 0;
            int utf8len = FcUtf8ToUcs4((const FcChar8*)text + i, &cs4, textlen - i);
            if (cs4 == 0 || utf8len < 0) {
                WARN("Invalid input, make sure it is UTF-8");
                return NULL;
            }
            i += utf8len;
            FcCharSetAddChar(cs, cs4);
        }

        pat = FcPatternBuild(NULL, FC_CHARSET, FcTypeCharSet, cs, NULL);
        if (!pat) RETURN_DEFER(NULL);

        FcResult match_result;
        FcDefaultSubstitute(pat);
        matches = FcFontSort(NULL, pat, FcFalse, &cs, &match_result);
        if (!matches) RETURN_DEFER(NULL);

        char *font_path = NULL;
        for (int i = 0; i < matches->nfont; i++) {
            FcResult err = FcPatternGetString(matches->fonts[i], FC_FILE, 0, (FcChar8**)&font_path);
            if (err) continue;
            if (strstr(font_path, ".ttf") == NULL) {
                font_path = NULL;
            } else {
                break;
            }
        }
        RETURN_DEFER(font_path);
    }

defer:
    if (cs) FcCharSetDestroy(cs);
    if (pat) FcPatternDestroy(pat);
    if (matches) FcFontSetDestroy(matches);
    return result;
}


