#include "page.h"
#include "log.h"
#include "config.h"

void page_draw(page *page) {
    SDL_SetRenderDrawColor(g_rend.rend, bg.r, bg.g, bg.b, bg.a);
    SDL_RenderClear(g_rend.rend);
    for (size_t i = 0; i < page->length; i++)
        frame_draw(page->data + i);
}

void page_draw_pdf(page *page, HPDF_Doc pdf) {
    HPDF_Page pp = HPDF_AddPage(pdf);
    HPDF_Page_SetWidth(pp, (float)g_rend.width);
    HPDF_Page_SetHeight(pp, (float)g_rend.height);
    
    HPDF_Page_SetRGBFill(pp, bg.r / 255.0f, 
            bg.g / 255.0f, bg.b / 255.0f);
    HPDF_Page_Rectangle(pp, 0, 0, g_rend.width, g_rend.height);
    HPDF_Page_Fill(pp);
    HPDF_Page_SetRGBFill(pp, fg.r / 255.0f, 
            fg.g / 255.0f, fg.b / 255.0f);

    for (size_t i = 0; i < page->length; i++)
        frame_draw_pdf(page->data + i, pdf, pp);
}

