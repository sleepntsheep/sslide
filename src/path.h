#pragma once
#include <stdbool.h>
#include <stddef.h>
#include <ena/lstr.h>

bool path_isrelative(const char *path);
lstr path_gethome(void);
lstr path_dirname(const char *path);

