#pragma once
#ifndef PAGE_H_
#define PAGE_H_

#include <stddef.h>
#include "renderer.h"
#include "frame.h"
#include <hpdf.h>

#define l_con page
#define l_val struct frame
#define l_valdrop frame_drop
#define l_valclone frame_clone
#include <ena/dynarray.h> 

void page_draw(page *page);
void page_draw_pdf(page *page, HPDF_Doc pdf);

#endif

