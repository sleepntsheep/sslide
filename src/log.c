#include "log.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdarg.h>
#include <assert.h>
#include <errno.h>

#ifndef NO_SYSLOG
#include <syslog.h>
#endif

static const struct {
    const char name[9];
    const char prefix[9];
    int syslog_equivalent;
} log_level_map[] = {
    [SSLIDE_LOG_NONE] = {"none", "none", -1},
    [SSLIDE_LOG_PANIC] = {"panic", "panic", LOG_ERR},
    [SSLIDE_LOG_WARN] = {"warn", "warn", LOG_WARNING},
    [SSLIDE_LOG_INFO] = {"info", "info", LOG_INFO},
    [SSLIDE_LOG_DEBUG] = {"debug", "dbg", LOG_DEBUG},
};

static int do_syslog = 0;
static enum log_level log_level = SSLIDE_LOG_NONE;

void log_init(int llevel, int ldo_syslog) {
    /* 
     * set environment SSLIDE_TRACE to one of 
     * DEBUG
     * INFO
     * WARN
     * PANIC
     */

#if 0
    char *s = getenv("SSLIDE_TRACE");
    if (!s) return;
    for (size_t i = 0; i < SSLIDE_LOG_LEVEL_COUNT; i++) {
        if (strcmp(s, log_level_map[i].name) == 0) {
            level = i;
            break;
        }
    }
#endif
    log_level = llevel;

#ifndef NO_SYSLOG
    if (ldo_syslog && llevel != SSLIDE_LOG_NONE)
    {
        openlog("sslide", LOG_PID, LOG_USER);
        setlogmask(LOG_UPTO(log_level_map[llevel].syslog_equivalent));
    }
#endif
}

void log_deinit(void)
{
#ifndef NO_SYSLOG
    if (do_syslog)
        closelog();
#endif
}

static void syslog_msg(enum log_level type, const char *fmt, va_list va)
{
#ifndef NO_SYSLOG
    if (!do_syslog) return;
    assert(type != SSLIDE_LOG_NONE);
    int slvl = log_level_map[type].syslog_equivalent;
    char msg[4096];
    int n = vsnprintf(msg, sizeof msg, fmt, va);
    (void)n;

    syslog(slvl, "%s", msg);
#endif
}

void log_msg(enum log_level type, const char *file, int line, const char *fmt, ...)
{
    if (log_level > type) return;
    fprintf(stderr, "%s:%d [%s]: ", file, line, log_level_map[type].prefix);
    va_list va, va2;
    va_start(va, fmt);
    va_copy(va2, va);
    vfprintf(stderr, fmt, va);
    va_end(va);
    fputc('\n', stderr);

    syslog_msg(type, fmt, va2);
    va_end(va2);
}

