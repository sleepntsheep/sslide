#pragma once

#ifndef CONFIG_H_
#define CONFIG_H_
#include <SDL2/SDL.h>

extern const int margin_x;
extern const int margin_y;
extern const int progress_bar_height;
extern const int linespacing ;
extern SDL_Color fg;
extern SDL_Color bg;

#endif

