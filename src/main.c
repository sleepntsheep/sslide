/* sslide - sleepntsheep 2022 */
#include <errno.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <fontconfig/fontconfig.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <hpdf.h>

#include "tinyfiledialogs.h"

#include "log.h"
#include "config.h"
#include "page.h"
#include "frame.h"
#include "font.h"
#include "string.h"
#include "image.h"
#include "path.h"
#include "slide.h"

#define VERSION "0.1.0"

static void pdf_error_handler  (HPDF_STATUS   error_no,
                HPDF_STATUS   detail_no,
                void         *user_data)
{
    (void)user_data;
    PANIC("pdf: error_no=%04X, detail_no=%u\n%s", (HPDF_UINT)error_no,
                (HPDF_UINT)detail_no, strerror(detail_no));
}

void usage(char *argv0)
{
    INFO("Usage: %s [OPTIONS] <FILE>\n", argv0);
    exit(0);
}

void drawprogressbar(float progress /* value between 0 and 1 */)
{
    if (progress_bar_height == 0) return;
    SDL_SetRenderDrawColor(g_rend.rend, fg.r, fg.g, fg.b, fg.a);
    SDL_RenderFillRect(g_rend.rend, &(SDL_Rect){
        .x = 0,
        .y = g_rend.height - progress_bar_height,
        .w = progress * g_rend.width,
        .h = progress_bar_height,
    });
}

void pdf(slide *slide)
{
    lstr home = path_gethome();
    if (!home) home = lstr_make(".");
    const char *save_path = tinyfd_saveFileDialog("PDF save path", home, 0, NULL, "pdf file");
    lstr_drop(&home);
    if (!save_path) return;
    HPDF_Doc pdf = HPDF_New(pdf_error_handler, NULL);
    HPDF_UseUTFEncodings(pdf);
    HPDF_SetCompressionMode(pdf, HPDF_COMP_ALL);
    for (size_t i = 0; i < slide->length; i++)
        page_draw_pdf(&slide->data[i], pdf);
    HPDF_SetPageMode(pdf, HPDF_PAGE_MODE_FULL_SCREEN);
    HPDF_SaveToFile(pdf, save_path);
    HPDF_Free(pdf);
}

void run(slide *slide)
{
    size_t cur = 0;
    if (slide->length == 0)
        return;

    bool redraw = true;
    while (true) {
        SDL_Event event;
        /* event loop */
        while (SDL_PollEvent(&event)) {
            switch (event.type) {
            case SDL_QUIT:
                return;
            case SDL_KEYDOWN:
                switch (event.key.keysym.sym) {
                case SDLK_0:
                case SDLK_1:
                case SDLK_2:
                case SDLK_3:
                case SDLK_4:
                case SDLK_5:
                case SDLK_6:
                case SDLK_7:
                case SDLK_8:
                case SDLK_9:
                    cur = (event.key.keysym.sym - SDLK_0) *
                            (slide->length - 1) / 9.0L;
                    redraw = true;
                    break;
                case SDLK_e:
                {
                    pdf(slide);
                    break;
                }
                case SDLK_UP:
                case SDLK_LEFT:
                case SDLK_k:
                    if (cur) cur--;
                    redraw = true;
                    break;
                case SDLK_DOWN:
                case SDLK_RIGHT:
                case SDLK_j:
                    if (cur + 1 < slide->length)
                        cur++;
                    redraw = true;
                    break;
                case SDLK_u:
                    cur = 0;
                    redraw = true;
                    break;
                case SDLK_d:
                    cur = slide->length - 1;
                    redraw = true;
                    break;
                case SDLK_i: {
                    SDL_Color tmp = bg;
                    bg = fg;
                    fg = tmp;
                    redraw = true;
                    break;
                             }
                default:
                    break;
                }
                break;
            case SDL_WINDOWEVENT:
                switch (event.window.event) {
                case SDL_WINDOWEVENT_RESIZED:
                case SDL_WINDOWEVENT_SIZE_CHANGED:
                case SDL_WINDOWEVENT_MAXIMIZED:
                    g_rend.width = event.window.data1;
                    g_rend.height = event.window.data2;
                    redraw = true;
                    break;
                case SDL_WINDOWEVENT_MOVED:
                case SDL_WINDOWEVENT_SHOWN:
                case SDL_WINDOWEVENT_EXPOSED:
                    redraw = true;
                    break;
                case SDL_WINDOWEVENT_CLOSE:
                    return;
                default:
                    break;
                }
                break;
            default:
                break;
            }
        }

        if (redraw) {
            page_draw(slide->data + cur);
            drawprogressbar((float)(cur + 1) / slide->length);
            SDL_RenderPresent(g_rend.rend);
            redraw = false;
        }
        SDL_Delay(15);
    }
}

int main(int argc, char **argv)
{
    static const char *log_level_env_map[] = {
        [SSLIDE_LOG_NONE] = "none",
        [SSLIDE_LOG_PANIC] = "panic",
        [SSLIDE_LOG_WARN] = "warn",
        [SSLIDE_LOG_INFO] = "info",
        [SSLIDE_LOG_DEBUG] = "debug",
    };
    char *log_level_env = getenv("SSLIDE_LOG");
    int log_level = SSLIDE_LOG_INFO;
    if (log_level_env)
    {
        for (size_t i = 0; i < SSLIDE_LOG_LEVEL_COUNT; i++) {
            if (strcmp(log_level_env, log_level_env_map[i]) == 0) {
                log_level = i;
                break;
            }
        }
    }
    log_init(log_level, 1);

    INFO("sslide version %s", VERSION);

    char *src = NULL;

    for (int i = 1; i < argc; i++)
        src = argv[i];

    if (NULL == src) {
        lstr home = path_gethome();
        src = tinyfd_openFileDialog("Open slide", home ? home : ".", 0, 0, 0, false);
        lstr_drop(&home);
        if (!src) usage(argv[0]);
    }

    renderer_init(src, 800, 600);

    slide slide = slide_parse(src);

    if (0 == slide.length) PANIC("slide has length 0");

    run(&slide);

    INFO("cleaning up");
    slide_drop(&slide);

    renderer_deinit();
    log_deinit();

    return EXIT_SUCCESS;
}

