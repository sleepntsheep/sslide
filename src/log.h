#pragma once
#ifndef LOG_H_
#define LOG_H_

enum log_level { 
    SSLIDE_LOG_DEBUG,
    SSLIDE_LOG_INFO,
    SSLIDE_LOG_WARN,
    SSLIDE_LOG_PANIC,
    SSLIDE_LOG_NONE,
    SSLIDE_LOG_LEVEL_COUNT,
};

void log_init(int llevel, int ldo_syslog);
void log_deinit(void);
void log_msg(enum log_level type, const char *file, int line, const char *fmt, ...);

#define DBG(...) \
    log_msg(SSLIDE_LOG_DEBUG, __FILE__, __LINE__, __VA_ARGS__)
#define INFO(...) \
    log_msg(SSLIDE_LOG_INFO, __FILE__, __LINE__, __VA_ARGS__)
#define WARN(...) \
    log_msg(SSLIDE_LOG_WARN, __FILE__, __LINE__, __VA_ARGS__)
#define PANIC(...) \
    do { log_msg(SSLIDE_LOG_PANIC,__FILE__, __LINE__, __VA_ARGS__); abort(); } while (0)

#endif /* LOG_H_ */

