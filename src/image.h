#pragma once
#ifndef SSLIDE_IMAGE_H
#define SSLIDE_IMAGE_H

#include <hpdf.h>
#include "renderer.h"
#include "rect.h"
#include <stdbool.h>

enum image_type {
    image_jpeg,
    image_png,
};

struct image {
    enum image_type type;
    SDL_Texture *texture;
    lstr path;
    float ratio;
    bool valid;
};

struct image image_init(const char*);
struct image image_clone(struct image);
void image_load_texture(struct image *);
void image_draw_pdf(struct image *image, 
        const struct rect *rect, HPDF_Doc pdf, HPDF_Page pp);
void image_draw(struct image *image,
        const struct rect *rect);
void image_drop(struct image *);

#endif /* SSLIDE_IMAGE_H */

